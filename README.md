# Setup

1. Copy .env.sample to .env
1. Make changes to .env
1. Run `docker-compose up -d`
