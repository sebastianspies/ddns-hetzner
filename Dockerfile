FROM alpine:3.13

RUN apk add --no-cache bash bind-tools curl

COPY ./init.sh /

CMD ./init.sh
